<?php

namespace Laravist\Hasher;

use Illuminate\Support\ServiceProvider;

class md5Provider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('md5hash',function(){
            return new Md5Hasher();
        });//用法 app('md5hash')->make('password',['salt'=>'miya'])
    }
}
