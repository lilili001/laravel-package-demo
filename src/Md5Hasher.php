<?php
/**
 * Created by PhpStorm.
 * User: miyaye
 * Date: 17/10/22
 * Time: 下午9:19
 */

namespace Laravist\Hasher;


/**
 * Class Md5Hasher
 * @package Laravist\Hasher
 */
class Md5Hasher
{
    /**
     * @param $value
     * @param array $options
     * @return string
     */
    public function make($value, $options=[])
    {
        $salt = isset( $options['salt'] ) ? $options['salt'] : '';
        return hash('md5'.$value.$salt);
    }

    /**
     * @param $value
     * @param $hashvalue
     * @param array $options
     * @return bool
     */
    public function check($value, $hashvalue, array $options=[])
    {
        $salt = isset( $options['salt'] ) ? $options['salt'] : '';
        return hash('md5'.$value.$salt) == $hashvalue;
    }
}